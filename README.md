# 开发设置
## 设置Mysql
为了开发方便大家使用相同的用户名和数据库名
```python
'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'wistomgraden',
        'USER': 'root',
        'PASSWORD': '123456',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
```
数据集utf8，一般情况下应该够用
## 安装``requirements.txt``环境依赖
**建议在虚拟环境下安装**
```txt
pip install -r requirements.txt
#用于生成
pip freeze > requirements.txt
```

from datetime import datetime

from django.db import models


class AddStreetInformation(models.Model):
    street_id = models.IntegerField(primary_key=True, verbose_name=u"街道id")
    administrative_code = models.IntegerField(verbose_name=u"行政区划代码")
    street_name = models.CharField(max_length=100, blank=False, verbose_name=u"街道名称")
    core_coordinate_x = models.FloatField(verbose_name=u"核心区坐标x")
    core_coordinate_y = models.FloatField(verbose_name=u"核心区坐标y")
    contacts = models.CharField(max_length=50, blank=False, verbose_name=u"联系人")
    tell_phone = models.CharField(max_length=15, blank=False, verbose_name=u"联系电话")
    note = models.CharField(max_length=200, verbose_name=u"备注")

    class Meta:
        verbose_name = u"街道信息管理"
        verbose_name_plural = verbose_name


class AddCommunityInformation(models.Model):
    community_id = models.IntegerField(primary_key=True, verbose_name=u"社区id")
    counties_id = models.CharField(max_length=100, verbose_name=u"所属区县代号")
    street_id = models.ForeignKey(AddStreetInformation, on_delete=models.CASCADE,verbose_name=u"所属街道id")
    community_name = models.CharField(max_length=100, blank=False, verbose_name=u"社区名称")
    note = models.CharField(max_length=200, verbose_name=u"备注")

    class Meta:
        verbose_name = u"社区信息管理"
        verbose_name_plural = verbose_name


class AddGreedBelt(models.Model):
    green_belt_id = models.IntegerField(primary_key=True, verbose_name=u"")
    green_belt_name = models.CharField(max_length=100, blank=False, verbose_name=u"绿地名称")
    data_classification = models.CharField(max_length=100, verbose_name=u"数据分类")
    green_belt_category = models.CharField(max_length=100, verbose_name=u"绿地类别")
    maintenance_level = models.CharField(max_length=100, verbose_name=u"养护级别")
    street_id = models.ForeignKey(AddStreetInformation, on_delete=models.CASCADE, verbose_name=u"所属街道id")
    completion_time = models.DateTimeField(verbose_name=u"建成时间")
    green_belt_Area = models.FloatField(verbose_name=u"公共绿地总面积")
    water_area = models.FloatField(verbose_name=u"水面积")
    land_area = models.FloatField(verbose_name=u"陆地面积")
    green_area = models.FloatField(verbose_name=u"绿化面积")
    building_area = models.FloatField(verbose_name=u"建筑占地面积")
    construction_area = models.FloatField(verbose_name=u"建筑面积")
    old_building_area= models.FloatField(verbose_name=u"古建面积")
    cover_area = models.FloatField(verbose_name=u"铺装面积")
    other_unit_area = models.FloatField(verbose_name=u"外单位占地面积")
    other_area = models.FloatField(verbose_name=u"其他占地面积")
    green_coverage_area = models.FloatField(verbose_name=u"绿化覆盖面积")
    contacts = models.CharField(max_length=50, verbose_name=u"联系人")
    note = models.CharField(max_length=200, verbose_name=u"备注")

    class Meta:
        verbose_name = u"绿地基础信息管理"
        verbose_name_plural = verbose_name


class AddGreenFieldsSurveyInformation(models.Model):
    survey_info_id = models.IntegerField(primary_key=True, verbose_name=u"调查信息id")
    # tree_species = models   树木信息表外键
    category = models.CharField(max_length=100, verbose_name=u"类别")
    height = models.FloatField(verbose_name=u"树高")
    diameter = models.FloatField(verbose_name=u"胸径")
    ground_diameter = models.FloatField(verbose_name=u"地径")
    crown = models.FloatField(verbose_name=u"冠幅")
    amount = models.FloatField(verbose_name=u"数量")
    area = models.FloatField(verbose_name=u"面积")
    user_id = models.CharField(max_length=20, verbose_name=u"用户id")
    update_time = models.DateTimeField(verbose_name=u"更新日期")
    note = models.CharField(max_length=100, verbose_name=u"备注")

    class Meta:
        verbose_name = u"绿地调查数据管理"
        verbose_name_plural = verbose_name


class SearchTree(models.Model):
    green_belt_id = models.IntegerField(verbose_name=u"绿地id")
    green_belt_name = models.CharField(max_length=100, verbose_name=u"绿地名称")
    classification = models.CharField(max_length=100, verbose_name=u"分类")
    maintenance_level = models.CharField(max_length=10, verbose_name=u"养护级别")
    total = models.IntegerField(verbose_name=u"合计")

    class Meta:
        verbose_name = u"查找树木"
        verbose_name_plural = verbose_name
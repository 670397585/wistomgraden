import xadmin
from xadmin import views
from xadmin.plugins.auth import UserAdmin


from .models import AddStreetInformation, AddCommunityInformation, AddGreedBelt, AddGreenFieldsSurveyInformation, SearchTree


class BaseSetting(object):
    enable_themes = True
    use_bootswatch = True


class GlobalSetting(object):
    site_title = "智慧园林"
    site_footer = "智慧园林网"
    menu_style = "accordion"


class AddStreetInformationAdmin(object):
    list_display = ['street_id', 'administrative_code', 'street_name', 'contacts', 'tell_phone', 'note']
    search_fields = ['street_id', 'administrative_code', 'street_name', 'contacts', 'tell_phone', 'note']
    list_filter = ['street_id', 'administrative_code', 'street_name', 'contacts', 'tell_phone', 'note']


class AddCommunityInformationAdmin(object):
    list_display = []
    search_fields = []
    list_filter = []


class AddGreedBeltAdmin(object):
    list_display = []
    search_fields = []
    list_filter = []


class AddGreenFieldsSurveyInformationAdmin(object):
    list_display = []
    search_fields = []
    list_filter = []


class SearchTreeAdmin(object):
    list_display = []
    search_fields = []
    list_filter = []


xadmin.site.register(AddStreetInformation, AddStreetInformationAdmin)
xadmin.site.register(AddCommunityInformation, AddCommunityInformationAdmin)
xadmin.site.register(AddGreedBelt, AddGreedBeltAdmin)
xadmin.site.register(AddGreenFieldsSurveyInformation, AddGreenFieldsSurveyInformationAdmin)
xadmin.site.register(SearchTree, SearchTreeAdmin)
xadmin.site.register(views.BaseAdminView, BaseSetting)
xadmin.site.register(views.CommAdminView, GlobalSetting)